<?php
require('Controller.php');
require('view/UploadActivityView.php');
require('view/ErrorView.php');


class UploadActivityController implements Controller {

    public function handle($request){

      		$view = new UploadActivityView();
      		$html = $view->render();

      		return $html;

    }
}
?>
