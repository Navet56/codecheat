<?php
 class Account{
   private $name;
   private $first_name;
   private $birthdate;
   private $sex;
   private $height;
   private $weight;
   private $email; //primary key in the DataBase
   private $password;

   public function  __construct() { }
   
   public function init($n, $p, $b, $s, $h, $w, $e, $pw){
     $this->name = $n;
     $this->first_name = $p;
     $this->birthdate = $b;
     $this->sex = $s;
     $this->height = $h;
     $this->weight = $w;
     $this->email = $e;
     $this->password = $pw;
   }
   
   public function getLastname(){ return $this->name; }
   public function getFirstname(){ return $this->first_name; }
   public function getBirthdate(){ return $this->birthdate; }
   public function getSex(){ return $this->sex; }
   public function getHeight(){ return $this->height; }
   public function getWeight(){ return $this->weight; }
   public function getEmail(){ return $this->email; }
   public function getPassword(){ return $this->password; }


  }
?>
