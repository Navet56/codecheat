<?php

	require_once("BaseView.php");
    class ConnectUserForm {

        public static function render() {
            $html = BaseView::render(false) . "
              <form action=\"?page=validconnectuser\" method=\"post\">
             <div class=\"connectcompte\">
               <h1>Connectez vous</h1>
               <p>Merci de remplir ce formulaire pour vous connecter</p>
               <hr>

               <label for=\"email\"><b>Email</b></label>
               <input type=\"text\" placeholder=\"Entrer votre mail\" name=\"email\" required>
            
            
               <label for=\"psw\"><b>Mot de passe</b></label>
               <input type=\"password\" placeholder=\"Entrez votre mot de passe\" name=\"psw\">

         
               <div>
                 <button type=\"submit\" class=\"connectbtn\">Se connecter</button>
               </div>
             </div>
            </form>
            </body>
            </html>
            ";

            return $html;
        }

    }

?>
