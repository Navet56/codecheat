<?php
require('Controller.php');
require('view/ValidConnectView.php');
require('view/ErrorView.php');
require('model/AccountDAO.php');
//require('model/Account.php');

class ValidConnectUserController implements Controller {

    public function handle($request){

        $allUser = AccountDAO::getInstance()->findAll();
        foreach ($allUser as $userKey => $user){
			if ((strpos($user->getEmail(), $request['email']) !== false) && (strpos($user->getPassword(), $request['psw']) !== false)) {
				$_SESSION['login'] = $request['email'];
				$view = new ValidConnectView();
				return $view->render("a reussi");
			}
		}
		$view = new ValidConnectView();
		$html = $view->render("a échoué");
				
		return $html;
    }
}
?>
