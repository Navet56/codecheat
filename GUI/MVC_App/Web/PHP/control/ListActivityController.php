<?php
require('Controller.php');
require_once('model/ActivityDAO.php');
require('view/ListActivityView.php');

class ListActivityController implements Controller{

    private function arrayToHTML($array){

        $header = current($array);

        if (!is_array($header) || empty($header)) {
            return "Aucune activité dans la base";
        }

        $table = '<table><thead><tr>';

                
        $table .= '<th> Date    </th>
        ';
        $table .= '<th> Description </th>
        ';
        $table .= '<th> Heure de début </th>
        ';
        $table .= '<th> Durée    </th>
        ';
        $table .= '<th> Distance </th>
        ';
        $table .= '<th> Cardio Min </th>
        ';
        $table .= '<th> Cardio Moyenne </th>
        ';
        $table .= '<th> Cardio Max </th>
        ';
        $table .= '<th> Détails  </th>
        ';
                

        $table .= '</tr>
        </thead>
        <tbody>
        ';
        foreach ($array as $item) {
            $i = 0;
            if($item['my_account'] == $_SESSION['login']){
                $table .= '<tr>
                ';
				foreach ($header as $key => $val) {

                    if ($key != 'act_id' && $key != 'my_account') {
                        if (!isset($item[$key])) {

                            $table .= '<td>null' . $i . '</td>' ;
                                
                        } else {
    
                            $table .= '<td>' .(string) $item[$key] . '</td>
                            ' ;
    
                                    
                        }
                                
                        $i++;
                    }
							
					
                }
                $myKey = $item['act_id'];
                $table .= '<td> <a href="?page=selectactivity&id=' . $myKey . '"> <img alt="arrow" src="arrow.png" width=150" height="70"> </a> </td>';
                $table .= '</tr>
            ';
            }


            
        }

        return $table . '</tbody>
        </table>
        ';
    }

    public function handle($request){
    	$view = new ListActivityView();
    	return $view->render($this->arrayToHTML((array) ActivityDAO::getInstance()->findAll()));
    }

}
?>
