#include "Human.hpp"
#include <cstdio>
#include <iostream>

//Constructeur
Human::Human(){
    this->nom = "Test";
    std::cout << "Naissance de l'humain " << this->nom << std::endl;
}

//Destructeur
Human::~Human(){
    std::cout << "Mort de l'humain" << std::endl;
}

void Human::methode(){
    std::cout << "Une methode de la classe Human" << std::endl;
}

//getter
std::string Human::getName() const {
    return this->nom;
}
