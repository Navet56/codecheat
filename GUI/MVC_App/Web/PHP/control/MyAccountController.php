<?php
require('Controller.php');
require('view/MyAccountView.php');
require('model/AccountDAO.php');


    class MyAccountController implements Controller{

        public function handle($request){
            $view = new MyAccountView();
            $accountArray = [$_SESSION['login']];
            $myUser = AccountDAO::getInstance()->findUser($accountArray[0])[0];
            
            $name = $myUser->getLastname();
			$firstName = $myUser->getFirstname();
			$height = $myUser->getHeight();
            $weight = $myUser->getWeight();
            $accountArray[] = $name;
            $accountArray[] = $firstName;
            $accountArray[] = $height;
            $accountArray[] = $weight;

            
			return $view->render($accountArray);
        }
    }
?>
