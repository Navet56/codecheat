DELETE FROM Data;
DELETE FROM Activity;
DELETE FROM Account;

DROP TABLE Account;
DROP TABLE Activity;
DROP TABLE Data;

CREATE TABLE Account(
    name VARCHAR(15),
    first_name VARCHAR(15),
    email VARCHAR(25) PRIMARY KEY,
    sex BOOLEAN,
    height INTEGER(3) CHECK (height > 0),
    weight INTEGER(3) CHECK (weight > 0),
    password VARCHAR(30) CHECK (length(password) > 7 AND length(password) <= 30),
    birthdate TEXT
);

CREATE TABLE Activity(
    act_id INTEGER PRIMARY KEY AUTOINCREMENT,
    date TEXT,
    description TEXT,
    heureDeDebut TEXT,
    duree INTEGER,
    distance FLOAT,
    cardio_min FLOAT,
    cardio_moyenne FLOAT,
    cardio_max FLOAT,
    my_account INTEGER(5) NOT NULL,
    FOREIGN KEY(my_account) REFERENCES Account(email)
);

CREATE TABLE Data(
    data_id INTEGER PRIMARY KEY AUTOINCREMENT,
    time TEXT,
    cardio_frequency INTEGER(4) CHECK (cardio_frequency > 0),
    longitude FLOAT,
    latitude FLOAT,
    altitude FLOAT,
    my_activity INTEGER(5) NOT NULL,
    FOREIGN KEY(my_activity) REFERENCES Activity(act_id)
);

