<?php
require('Controller.php');
require('view/ErrorView.php');

class ErrorController implements Controller{

    public function handle($request){
		$view = new ErrorView();
		return $view->render(false);
    }
}
?>
 
