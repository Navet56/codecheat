package control;

import view.*;
import model.*;

/**
 * Generic Controller class
 */
public class Controller {
    private View view ;
    private Model model;

    /**
     * Controller constructor
     * This constructor creates a new View instance
     * @param model a generic Model
     */
    public Controller(Model model) {
        if (model != null){
          this.model = model;
          this.view = new View(this.model);
        }
        else System.out.println("Controller() : model is null");
    }

    /**
     * This method initializes the GUI and set the different listeners
     */
    public void showGUI(){
        this.view.start();

        //Listeners definition
        MainMenu mainMenu = this.view.getMainFrame().getMenu();
        MainPanel mainPanel = this.view.getMainFrame().getMainPanel();
        mainMenu.getQuit().addActionListener(e -> System.exit(0));
        mainMenu.getAbout().addActionListener( e -> new AboutFrame(Model.TITLE + " by " + Model.AUTHOR, Model.VERSION, Model.LICENSE)); //this is a lambda function, see more in docs.oracle.com
        mainMenu.getNewFile().addActionListener(new LabelListener(mainPanel.getLabel1(), this.view.getModel().getTextNewFile()));
        mainPanel.getLabel1().setText(this.model.getTextLabel1());
        mainPanel.getLabelo().setText(this.model.getTextLabelo());


    }
}
