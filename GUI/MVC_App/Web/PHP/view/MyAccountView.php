<?php
	require_once('model/Activity.php');
	require_once("BaseView.php");
    class MyAccountView {

        public static function render($response) {
            $html = BaseView::render(true) . 
			"
             <form action=\"?page=validmodif\" method=\"post\">
      <div class=\"modifcompte\">
        <h1>Modifier son compte</h1>
        <p>Changez les champs que vous voulez puis cliquez sur \"Modifier le compte\"</p>
        <hr>
            
        <label for=\"nom\"><b>Nom</b></label>
        <input type=\"text\" placeholder=\"" . $response[1] . "\" name=\"nom\" pattern=\"[A-Z](?=.*[a-z]).{2,30}\">
            
        <label for=\"prenom\"><b>Prenom</b></label>
        <input type=\"text\" placeholder=\"" . $response[2] . "\" name=\"prenom\" >
            
        <label for=\"sexe\"><b>Sexe</b></label>
        <select id=\"sexe\" name=\"sexe\">
          <option value=\"masculin\"> Masculin</option>
          <option value=\"feminin\"> Feminin</option>
          <option value=\"autre\"> Autre </option>
        </select>
        <br>
        <label for=\"taille\" ><b>Taille</b></label>
        <input type=\"number\" placeholder=\"" . $response[3] . "\" id=\"taille\" name=\"taille\" min=\"100\" max=\"272\">
            
        <label for=\"poids\" ><b>Poids</b></label>
        <input type=\"number\" placeholder=\"" . $response[4] . "\" id=\"poids\" name=\"poids\" min=\"0\" max=\"300\">
            
            
        <label for=\"psw\"><b>Mot de passe</b></label>
        <input type=\"password\" placeholder=\"Aucune modification\" name=\"mdp\" minlength=7 maxlength=30  pattern=\"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{7,30}\" required>
            
        <label for=\"psw-repeat\"><b>Entrer à nouveau le mot de passe</b></label>
        <input type=\"password\" placeholder=\"Même mot de passe\" name=\"repeat-mdp\" required>
            
        <label for=\"date\"><b>Date de naissance</b></label>
        <input type=\"date\" name=\"date\" required>
        
        <p>En créant ce compte vous acceptez les <a href=\"conditions\">conditions d'utilisations</a>.</p>
            
        <div>
          <button type=\"submit\" class=\"modifcomptebtn\">Modifier son compte</button>
        </div>
            </body>
            </html>
            ";

            return $html;
        }

    }

?>
