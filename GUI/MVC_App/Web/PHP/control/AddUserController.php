<?php
require('Controller.php');
require('view/AddUserForm.php');
class AddUserController implements Controller {

  public function handle($request){
    $view = new AddUserForm();
    $html = $view->render();

    return $html;
  }
}
?>
