package view;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.HeadlessException;

/**
 * The Main Frame class
 */
public class MainFrame extends JFrame {

    private final MainPanel mainPanel;
    private final MainMenu menu;

    /**
     * Main Frame constructor
     * @param title the Window title
     * @throws HeadlessException
     */
    public MainFrame(String title) throws HeadlessException {
        super(title);
        this.setLayout(new BorderLayout());
        this.setSize(1280,720);
        this.menu = new MainMenu();
        this.mainPanel = new MainPanel();
        this.setJMenuBar(this.menu);
        this.add(this.mainPanel, BorderLayout.CENTER);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public MainMenu getMenu() {
        return menu;
    }

    public MainPanel getMainPanel() {
        return this.mainPanel;
    }
}
