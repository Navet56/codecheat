<?php
    $src    = 'image.jpeg';
    $im     = imagecreatefrompng($src);
    $size   = getimagesize($src);
    $nb = 0;
    for($x=0;$x<$size[0];$x++){
        for($y=0;$y<$size[1];$y++){
            $rgb = imagecolorat($im, $x, $y);
            $r = ($rgb >> 16) & 0xFF;
            $g = ($rgb >> 8) & 0xFF;
            $b = $rgb & 0xFF;
            if($r != 0 || $g != 0 || $b !=0)
                $nb++;
        }
    }
    echo $nb;
?>
