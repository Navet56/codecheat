<?php
require_once('BaseView.php');
class WelcomeView {

    public static function render($connect) {
        $html = BaseView::render($connect) . "
             
			<center><h1>Bienvenue sur SportTrack!</h1></center>
            <div>
               <center> SportTrack vous permet de suivre vos performances sportives au fil du temps.
                <br>
                Et grâce à l'application SportTrack sur smartphone, ce suivi peut même se faire en temps réel!
                
                <div>
					<img src=\"sportrack.png\"/></center>
                </div>
             </div>
            <footer>
                Par Cedric Simar (B2) et Evan Diberder (B1)
            </footer>
            </body>
            </html>
            ";

        return $html;
    }

}

?>
