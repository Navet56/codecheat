def sortRec(t, indL, indR):
    """Methode de tri recursive selon le principe de separation. La
    methode s'appelle elle meme sur les tableaux gauche et droite
    par rapport a un pivot.

    Parameters
    ----------
    tab : list
        le tableau sur lequel est effectue la separation
    indL : int
        l'indice gauche de debut de tableau
    indR : int
        l'indice droit de fin de tableau

    Returns
    -------
    list
        le tableau trié
    """
    indS = indicePartition(t, indL, indR)
    if indS - 1 > indL:
        sortRec(t, indL, indS - 1)
    if indS + 1 < indR:
        sortRec(t, indS + 1, indR)
    return t


def indicePartition(tab, indL, indR):
    """Cette methode renvoie l'indice de separation du tableau
    en 2 parties par placement du pivot a la bonne case.

    Parameters
    ----------
    tab : list
        le tableau des valeurs
    indR : int
        indice Right de fin de tableau
    indL : int
        indice Left de debut de tableau

    Returns
    -------
    int
        l'indice de separation du tableau
    """
    pivot = tab[indL]
    while indL != indR:
        if tab[indL] > tab[indR]:
            tab[indL], tab[indR] = tab[indR], tab[indL]

        if tab[indL] == pivot:
            indR -= 1
        else:
            indL += 1
    return indL


def sort(t):
    """Tri par ordre croissant d'un tableau selon la methode du tri
    rapide (QuickSort). On suppose que le tableau passé en parametre
    est cree et possede au moins une valeur (nbElem > 0).
    Cette methode appelle triRapideRec qui elle effecture reelement
    le tri rapide selon la methode de separation recursive.

    Parameters
    ----------
    leTab : list
        le tableau a trier par ordre croissant

    Returns
    -------
    int
        le nombre d'entier qui contient le tableau
    """
    return sortRec(t, 0, len(t) - 1)


tab = [3, 7, 21, 12, 0, 50, 3, 1, 8, 99]
sortedTab = sort(tab)
print("Python : ", sortedTab)
