public class Player {

  private int width;
  private int height;
  private int x;
  private int y;
  private Color color;


  public Player(width, height, color, x, y){
    this.width = width;
    this.height = height;
    this.color = color;
    this.x = x;
    this.y = y;
  }
}
