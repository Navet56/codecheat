<?php
require('Controller.php');
require('view/ValidUserView.php');
require('view/ErrorView.php');
require('model/AccountDAO.php');

class ValidUserController implements Controller {

	public function handle($request){
		$view = new ValidUserView();
		$html = $view->render();

		$usrAcnt = new Account();

		if (isset($_POST["email"]) && $_POST["email"] <> "") {
			$usrAcnt->init($_POST["nom"], $_POST["prenom"], $_POST["date"], $_POST["sexe"], $_POST["taille"], $_POST["poids"], $_POST["email"], $_POST["mdp"]);
			try {
				AccountDAO::getInstance()->insert($usrAcnt);
			} catch (PDOException $e){
				$view = new ErrorView();
				$html = $view->render(false);
			}
		}
			
		return $html;
	}
}
?>
