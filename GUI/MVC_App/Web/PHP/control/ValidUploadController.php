<?php
require('Controller.php');
require('view/ValidUploadView.php');
require('view/ErrorView.php');
require('model/ActivityEntryDAO.php');
require('model/ActivityEntry.php');
require('model/CalculDistanceImpl.php');

class ValidUploadController implements Controller {

	public function handle($request){
		$view = new ValidUploadView();
		$html = $view->render();
        

		if(isset($_SESSION['login'])){

			if (isset($_FILES['fileToUpload']['tmp_name'])) {

				$login = $_SESSION['login'];


				try {

					$file = $_FILES['fileToUpload']['tmp_name']; if(empty($file)) throw new TypeError();

					$activity = new Activity();
					$array_json_data = CalculDistanceImpl::readData($file);
					$array_json_act = CalculDistanceImpl::makeActivityArray($file);

				} catch (TypeError $e) {

					return "<br> <br> <br> <br>Aucun fichier importé !" . (new ErrorView())->render(true);
				
				}
				
				$activity->init($array_json_act[0], $array_json_act[1], $array_json_act[2], $array_json_act[3], $array_json_act[4], $array_json_act[5], $array_json_act[6], $array_json_act[7], $login);

				try {
				
				$act_id = ActivityDAO::getInstance()->insert($activity);


				for ($i = 0; $i < sizeof($array_json_data) ; $i++) {
					$arrayI = $array_json_data[$i];
					$time = $arrayI['time'];
					$cardio = $arrayI['cardio_frequency'];
					$longitude = $arrayI['longitude'];
					$latitude = $arrayI['latitude'];
					$altitude = $arrayI['altitude'];

					$entry = new ActivityEntry();
					$entry->init($time, $cardio, $longitude, $latitude, $altitude, $act_id);
					ActivityEntryDAO::getInstance()->insert($entry);

				}

				} catch (PDOException $e){
					$view = new ErrorView();
					$html = $view->render(true);
				}

			}
			
		}

		return $html;
	}

}
?>
