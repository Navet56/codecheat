from tkinter import *
from tkinter.messagebox import  askokcancel
import model

def menu(fenetre):     #Création de la barre de menu avec ses boutons
    #"Barre de menu"
    top=Menu(fenetre)#on nome le menu "top"
    fenetre.config(menu=top)#on configure la fenetre pour que le menu de cette fenetre soit "top"
    F=Menu(top)#J est un sous-menu de top
    top.add_cascade(label='File',menu=F,underline=0)#création de la cascade File
    F.add_command(label='New file',command="",underline=0)
    F.add_command(label='Quit',command=quitter(fenetre),underline=0)
    E=Menu(top)#E est un sous-menu de top
    top.add_cascade(label='Edit',menu=E,underline=0)
    O=Menu(top)#etc
    top.add_cascade(label='Help',menu=O,underline=0)
    O.add_command(label='About',command=aPropos,underline=0)

def aPropos():         #Création de la section "à propos"
    msg =Toplevel()
    Message(msg, width=200, aspect=200, justify=CENTER,
        text ='''MVC Template in Python 3
        By Navet56
        Version 1.0''').pack(padx =15, pady =10)

def quitter(fenetre):       #créé la fenêtre  qui quitte le programme
        reponse=askokcancel(model.title,"Really quit ?") #on utilise la fonction askokcancel de la bibliothèque messagebox de tkinter
        if reponse:#si reponse == true (c'est a dire askokcancel renvoie true) (donc qu'on a cliqué sur ok)
                fenetre.destroy() #fermer la fenetre (la "detruire")
                exit()#on quitte la console

def showView() :
    fenetre = Tk(className=model.title) #Creation de la fenetre
    fenetre['bg']='white' # Couleur du fond de la fenetre en blanc
    fenetre.geometry("500x200") # Taille de la fenetre 500x200
    panel1 = Frame(fenetre) # Creation d'un panel
    panel2 = Frame(fenetre) # Creation d'un autre panel
    label1 = Label(panel1, text=model.data, bg='white', fg='black') # Creation d'un label (c'est à dire un texte)
    label1.pack() # Ajout de ce label au panel 1
    labelo = Label(panel2, text=model.data2, bg='white', fg='black') # Creation d'un nouveau label
    labelo.pack() # Ajout de ce label au panel 2
    panel1.pack(side=LEFT) # Ajout du panel 1 à gauche de la fenetre
    panel2.pack(side=RIGHT) # Ajout du panel 2 à droite de la fenetre
    bouton = Button(fenetre, text="Close", command=quitter(fenetre)) # Creation d'un bouton avec comme texte : "Fermer"
    bouton['bg'] = 'black'; # Couleur de l'arrière plan en blanc (couleur du fond)
    bouton['fg'] = 'white'; # Couleur de premier plan en blanc (couleur de la police)
    menu(fenetre)
    bouton.pack(side=BOTTOM) # Ajout du bouton dans la fenetre et collé tout en bas
    fenetre.mainloop() # Affichage de la fenetre
