from tkinter import *

fenetre = Tk()
fenetre.title("Drawing a triangle in Tkinter")

c = Canvas(fenetre, width = 800, height=800)

c.pack()

#Vertex 1 : x=200, y=600
#Vertex 2 : x=400, y=200
#Vertex 3 : x=600, y=600
points = [200,600,400,200,600,600]

c.create_polygon(points, fill="red")

fenetre.mainloop()
