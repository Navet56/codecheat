#include <iostream>
using namespace std;


  /**
  * Cette methode renvoie l'indice de separation du tableau
  * en 2 parties par placement du pivot a la bonne case.
  * @param tab le tableau des valeurs
  * @param indR indice Right de fin de tableau
  * @param indL indice Left de debut de tableau
  * @return l'indice de separation du tableau
  */
  int indicePartition(int tab[], int indL, int indR){
      int pivot = tab[indL];
      while(indL != indR){
          if(tab[indL] > tab[indR]){
              int tmp = tab[indL];
              tab[indL] = tab[indR];
              tab[indR] = tmp;
          }
          if(tab[indL] == pivot){
              indR--;
          } else{
              indL++;
          }
      }
      return indL;
  }

  /**
    * Methode de tri recursive selon le principe de separation. La
    * methode s'appelle elle meme sur les tableaux gauche et droite
    * par rapport a un pivot.
    * @param tab le tableau sur lequel est effectue la separation
    * @param indL l'indice gauche de debut de tableau
	  * @param indR l'indice droit de fin de tableau
    */
    int * sortRec(int t[], int indL, int indR){
  		int indS = indicePartition (t, indL, indR);

  		if ((indS-1) > indL){
  			sortRec(t, indL,(indS-1));
  		}
  		if ((indS+1) < indR){
  			sortRec(t, (indS+1), indR);
  		}
      return t;
	}


  /**
  * pour convertir un tableau en string
  */
  string arrayToString(int arr[], int size){
      string s = "[";
      int i;
      for (i = 0; i < size; i++){
        if(i < size-1)
          s += to_string(arr[i]) + ", ";
        else
          s += to_string(arr[i]);
      }
      s += "]";
      return s;
  }

	 /**
	  * Tri par ordre croissant d'un tableau selon la methode du tri
	  * rapide (QuickSort). On suppose que le tableau passé en parametre
	  * est cree et possede au moins une valeur (nbElem > 0).
	  * Cette methode appelle triRapideRec qui elle effecture reelement
	  * le tri rapide selon la methode de separation recursive.
	  * @param leTab le tableau a trier par ordre croissant
	  * @param nbElem le nombre d'entier qui contient le tableau
	  */
	  int * sort(int t[], int size){
		  return sortRec(t, 0, (size-1));
	  }

    int main() {
      int tab[] = {3,7,21,12,0,50,3,1,8,99};
      int size = sizeof(tab)/sizeof(tab[0]); //impossible de faire sizeof dans la fonction sort comme les autres langages, car le tableau change de type lors du passage dans une fonction, obliger donc de creer un parametre supplementaire à la fonction
      cout << "C++ : " << arrayToString(sort(tab, size), size) <<  endl;
      return 0;
    }
