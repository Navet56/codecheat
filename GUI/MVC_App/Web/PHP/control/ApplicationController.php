<?php
class ApplicationController{
    private static $instance;
    private $routes;

    private function __construct(){
        // Sets the controllers and the views of the application.
        $this->routes = [
            'userform' => ['controller'=>'AddUserController', 'view'=>'AddUserForm'],
            'validuser' => ['controller'=>'ValidUserController', 'view'=>'ValidUserView'],
            'connect' => ['controller'=>'ConnectUserController', 'view'=>'ConnectUserView'],
			'validconnectuser' => ['controller'=>'ValidConnectUserController', 'view'=>'ValidConnectView'],
            'listactivity' => ['controller'=>'ListActivityController', 'view'=>'ListActivityView'],
            'uploadactivity' => ['controller'=>'UploadActivityController', 'view'=>'UploadActivityView'],
            'disconnect' => ['controller'=>'DisconnectUserController', 'view'=>'DisconnectUserView'],
            'validupload' => ['controller'=>'ValidUploadController', 'view'=>'ValidUploadView'],
            'myaccount' => ['controller'=>'MyAccountController', 'view'=>'MyAccountView'],
            'validmodif' => ['controller'=>'ValidModifController', 'view'=>'ValidModifView'],
            'selectactivity' => ['controller'=>'SelectActivityController', 'view'=>'SelectActivityView'],
            'welcome' => ['controller'=>'WelcomeController', 'view'=>'WelcomeView'],
            'error' => ['controller'=>'ErroController', 'view'=>'ErrorView']

        ];
    }

    /**
     * Returns the single instance of this class.
     * @return ApplicationController the single instance of this class.
     */
    public static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance = new ApplicationController;
        }
        return self::$instance;
    }

    /**
     * Returns the controller that is responsible for processing the request
     * specified as parameter. The controller can be null if their is no data to
     * process.
     * @param Array $request The HTTP request.
     * @param Controller The controller that is responsible for processing the
     * request specified as parameter.
     */
    public function getController($request){
    if(array_key_exists($request['page'], $this->routes)){
    return $this->routes[$request['page']]['controller'];
}
    return null;
}

    /**
     * Returns the view that must be return in response of the HTTP request
     * specified as parameter.
     * @param Array $request The HTTP request.
     * @param Object The view that must be return in response of the HTTP request
     * specified as parameter.
     */
    public function getView($request){
    if( array_key_exists($request['page'], $this->routes)){
    return $this->routes[$request['page']]['view'];
}
    return $this->routes['error']['view'];
}
}
?>
