import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.scene.layout.FlowPane;
import javafx.util.Duration;

public class ImageMove extends Application {
   @Override
   public void start(Stage stage) {
      //Drawing an image
      Image image = new Image("mario.png", false);
      ImageView imageView = new ImageView(image);
      //Creating Translate Transition (animation en gros)
      TranslateTransition translateTransition = new TranslateTransition();

      //Setting the duration of the transition
      translateTransition.setDuration(Duration.millis(1300));

      //Setting the node for the transition
      translateTransition.setNode(imageView);

      //Setting the value of the transition along the x axis.
      translateTransition.setByX(500); //deplacement en x
      translateTransition.setByY(500); //en y

      //Setting the cycle count for the transition
      translateTransition.setCycleCount(100);

      //Setting auto reverse value to false
      translateTransition.setAutoReverse(false); //si on met true l'animation se reverse et donc boucle

      //Playing the animation
      translateTransition.play();

      FlowPane root = new FlowPane(); //On définit le root comme étant ici un FlowPane (pas le seul choix)
      root.setStyle("-fx-background-color: #00CC00;"); //set background color
      root.getChildren().add(imageView); //on add bien imageView et pas image, imageView etant bien une sous classe de Node donc c'est bon

      //Creating a scene object
      Scene scene = new Scene(root, 800, 800); // <=> Main JPanel

      //Setting title to the Stage
      stage.setTitle("Image Movement with JavaFX"); // JFRame <=> Stage

      //Adding scene to the stage
      stage.setScene(scene);

      //Displaying the contents of the stage
      stage.show();
   }
   public static void main(String args[]){
      launch(args); //lauch est une méthode de JavaFX
   }
}
