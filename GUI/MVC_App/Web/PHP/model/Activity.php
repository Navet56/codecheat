<?php
 class Activity{
   private $date;
   private $description;
   private $heureDebut;
   private $duree;
   private $distance;
   private $cardio_moyenne;
   private $cardio_min;
   private $cardio_max;
   private $my_account;

   public function  __construct() { }
   public function init($da, $de, $h, $du, $di, $cmo, $cmi, $cma, $a){
     $this->date = $da;
     $this->description = $de;
     $this->heureDebut = $h;
     $this->duree = $du;
     $this->distance = $di;
     $this->cardio_moyenne = $cmo;
     $this->cardio_min = $cmi;
     $this->cardio_max = $cma;
     $this->my_account = $a;


   }

   public function getDate(){ return $this->date; }
   public function getDescription(){ return $this->description; }
   public function getHeureDebut(){ return $this->heureDebut; }
   public function getDu(){ return $this->duree; }
   public function getDistance(){ return $this->distance; }
   public function getCardioMoyenne(){ return $this->cardio_moyenne; }
   public function getCardioMin(){ return $this->cardio_min; }
   public function getCardioMax(){ return $this->cardio_max; }
   public function getMyAccount(){ return $this->my_account; }
  }
?>
