#ifndef HEADER_FENPRINCIPALE
#define HEADER_FENPRINCIPALE

#include <QtWidgets>

class View : public QWidget
{
    Q_OBJECT

    public:
        View();

    private:
        QListView *vue;
        QStringListModel *modele;
        QPushButton *bouton;

    private slots:
        void clicSelection();
};


#endif
