<?php
require('Controller.php');
require('view/WelcomeView.php');
require('view/ErrorView.php');


class WelcomeController implements Controller {

    public function handle($request){

		if (isset($_SESSION['login'])){
			$connect = true;
		} else {
			$connect = false;
		}

      	$view = new WelcomeView();
      	$html = $view->render($connect);

      	return $html;

    }
}
?>
