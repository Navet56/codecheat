<?php
require('Controller.php');
require_once('view/SelectActivityView.php');
require_once('model/ActivityEntryDAO.php');

    class SelectActivityController implements Controller{

        public function handle($request){
			$view = new SelectActivityView();
    		return $view->render($this->arrayToHTML((array) ActivityEntryDAO::getInstance()->findAll()));
		}
		
		private function arrayToHTML($array){

			if(isset($_SESSION['login']) && isset($_REQUEST['id']) && ActivityDAO::getInstance()->findEmail($_REQUEST['id'])[0]['my_account'] == $_SESSION['login']){


				$header = current($array);

				if (!is_array($header) || empty($header)) {
					return "Aucune donnée dans la base";
				}


				$response = "Voici les informations supplémentaires de votre activité :
					";

				$activity = ActivityDAO::getInstance()->findFromKey($_REQUEST['id'])[0];

				$table1 = '<table><thead><tr>';

                
				$table1 .= '<th> Date    </th>
				';
				$table1 .= '<th> Description </th>
				';
				$table1 .= '<th> Heure de début </th>
				';
				$table1 .= '<th> Durée    </th>
				';
				$table1 .= '<th> Distance </th>
				';
				$table1 .= '<th> Cardio Min </th>
				';
				$table1 .= '<th> Cardio Moyenne </th>
				';
				$table1 .= '<th> Cardio Max </th>
				';

				$table1 .= '</tr>
				</thead>
				<tbody>
					<tr>';


				foreach ($activity as $key => $val) {
							
					if (!isset($key)) {

						$table1 .= '<td>null </td>' ;
							
					} else {

                        $table1 .= '<td>' .(string) $activity[$key] . '</td>
                        ' ;
                                
                    }
                            
				}
				
				$table1 .= '</tr>
				</tbody>
				</table>
				';

				

				$response .= $table1;


				



				$table = '<table><thead><tr>';

                
				$table .= '<th> Heure    </th>
				';
				$table .= '<th> Frequence Cardiaque </th>
				';
				$table .= '<th> Longitude </th>
				';
				$table .= '<th> Latitude    </th>
				';
				$table .= '<th> Altitude </th>
				';
				$table .= '</tr>
				</thead>
				<tbody>
				';

				foreach ($array as $item) {
					$i = 0;
					if($item['my_activity'] == $_REQUEST['id']){
						$table .= '<tr>
						';
						$myKey = '';
						foreach ($header as $key => $val) {

							if ($key != 'data_id' && $key != 'my_activity') {

								if (!isset($item[$key])) {

									$table .= '<td>null' . $i . '</td>' ;
										
								} else {
	
									$table .= '<td>' .(string) $item[$key] . '</td>
									' ;
	
									if($myKey == ''){
										$myKey = $item[$key];
									}
											
								}
										
								$i++;

							}
									
							
						}
						$table .= '</tr>
					';
					}


					
        		}


				$table .= '</tbody>
				</table>
				';

				$response .= $table;


			} else {
				$response = "Une erreur est survenue.";
			}
			return $response;
		}
    }
?>
