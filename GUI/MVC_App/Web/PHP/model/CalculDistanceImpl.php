<?php

require_once("CalculDistance.php");
class CalculDistanceImpl implements CalculDistance {
	private static $parcours;

 	public function calculDistance2PointsGPS($lat1, $long1, $lat2, $long2){
    	$R = 6378.137 * 1000;
    	$lat1 = deg2rad($lat1);
    	$lat2 = deg2rad($lat2);
    	$long1 = deg2rad($long1);
    	$long2 = deg2rad($long2);
    	$distance = (float) $R*acos(sin($lat2)*sin($lat1)+cos($lat2)*cos($lat1)*cos($long2-$long1));
    	return $distance;
	}

    public function calculDistanceTrajet(Array $parcours){
        $distance = 0.0;
        for ($i=0; $i < sizeof($parcours) - 1; $i++) {
        	$distance += $this->calculDistance2PointsGPS($parcours[$i]['latitude'], $parcours[$i]['longitude'], $parcours[$i+1]['latitude'], $parcours[$i+1]['longitude']);
        }
        return $distance;
    }

    public static function makeActivityArray($file){
		$obj = new CalculDistanceImpl();
		$datas = json_decode(file_get_contents($file), true);

		$act_info = $datas['activity'];

		$date = $act_info['date'];
		$desc = $act_info['description'];

		$dist =  $obj->calculDistanceTrajet($datas['data']);
		$heure_debut = $datas['data'][0]['time'];
		$heure_fin = $datas['data'][count($datas['data'])-1]['time'];

		$dateDebut = strToTime($heure_debut);
		$dateFin = strToTime($heure_fin);

		$duree = $dateFin - $dateDebut; 

		$cardio_array = [];
		for($i = 0; $i < sizeof($datas['data']); $i++){
			$cardio_array[] = $datas['data'][$i]['cardio_frequency'];
		}

		$cmin = min($cardio_array);
		$cmax = max($cardio_array);
		$cavg = array_sum($cardio_array)/count($cardio_array);


		$array = [$date, $desc, $heure_debut, $duree, $dist, $cavg, $cmin, $cmax];
			
		return $array;
    }

      
      
    public static function readActivity($file){
		$datas = json_decode(file_get_contents($file), true);
		$array = $datas['activity'];
		return $array;
    }
      
    public static function readData($file){
		$datas = json_decode(file_get_contents($file), true);
		$array = $datas['data'];
		return $array;
    }

      
}

?>
