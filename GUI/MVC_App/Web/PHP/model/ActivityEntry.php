<?php
 class ActivityEntry{
  private $time;
  private $cardio_frequency;
  private $longitude;
  private $latitude;
  private $altitude;
  private $my_activity;

  public function  __construct() { }
  public function init($t, $c, $lo, $la, $a, $ma){
    $this->time = $t;
    $this->cardio_frequency = $c;
    $this->longitude = $lo;
    $this->latitude = $la;
    $this->altitude = $a;
    $this->my_activity = $ma;
  }

  public function getTime(){ return $this->time; }
  public function getCardio(){ return $this->cardio_frequency; }
  public function getLongitude(){ return $this->longitude; }
  public function getLatitude(){ return $this->latitude; }
  public function getAltitude(){ return $this->altitude; }
  public function getMyActivity(){ return $this->my_activity; }
 }
?>
