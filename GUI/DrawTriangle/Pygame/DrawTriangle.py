import pygame, sys
from pygame.locals import *

pygame.init()

RED   = (255, 0, 0)
WHITE = (255, 255, 255)

FramePerSec = pygame.time.Clock()
fenetre = pygame.display.set_mode((800,800))
fenetre.fill(WHITE)
pygame.display.set_caption("Drawing a triangle with Pygame")
#Vertex 1 : x=200, y=600
#Vertex 2 : x=400, y=200
#Vertex 3 : x=600, y=600
points = [(200,600),(400,200),(600,600)]

pygame.draw.polygon( fenetre , RED  , points )

while(True) :

	pygame.display.update()
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()
	FramePerSec.tick(30)
