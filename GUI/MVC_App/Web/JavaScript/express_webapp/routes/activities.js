var express = require('express');
var router = express.Router();
var activity_dao = require('sport-track-db').activity_dao;
router.get("/", function(req, res, next){
   activity_dao.findAll(req.session.email, function(err, rows) {
      if(err != null){
        console.log("ERROR= " +err);
      } else if (req.session.email == undefined) {
        res.render('error', {});
      } else {
        res.render('activities', {data:rows});
      }
    });
});
module.exports = router;
