#if !defined(__HUMAN__)
#define __HUMAN__
#include <string>

class Human{
    public:
        Human();
        ~Human();
        void methode();
        std::string getName() const;

    private:
        std::string nom;
        std::string prenom;
        int age;
};

#endif