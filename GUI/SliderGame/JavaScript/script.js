/*
* Author : Navet56
* GPL 3 Licence
*/

//Creation de la fenetre
var screen = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.width = 800;
        this.canvas.height = 300;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.frameNo = 0;
        updateGameArea();
        },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

//Variables globales (à éviter le plus possible)
var player;
var obstacles = [];
var score;
var gameOver;
var jumping = false;
var speed = 5;
var playerSize = 30;

//Fonction qui retourne true toute les n frames
function everyinterval(n) {
    if ((screen.frameNo / n) % 1 == 0) return true;
    else return false;
}

//Represente le score
function Score(size, font, color, x, y){
  this.score = 0;
  this.size = size;
  this.font = font;
  this.color = color;
  this.x = x;
  this.y = y;
  this.text = "Score : " + Math.round(this.score);

  this.update = function(){
    this.score += 0.01;
    this.text = "Score : " + Math.round(this.score);
    ctx = screen.context;
    ctx.font = this.size + " " + this.font;
    ctx.fillStyle = color;
    ctx.fillText(this.text, this.x, this.y);
  }
}

//Represente une entité du jeu (player ou obstacle)
function Component(width, height, color, x, y) {
    this.width = width;
    this.height = height;
    this.color = color;
    this.x = x;
    this.y = y;
    this.gravity = 0;
    this.gravitySpeed = 0;

    this.update = function() {
        ctx = screen.context;
        ctx.fillStyle = color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }

    //Calcule la nouvelle position du joueur
    this.newPos = function() {
        this.gravitySpeed += this.gravity;
        this.y += this.gravitySpeed;
        let rockbottom = screen.canvas.height - this.height;
        if (this.y > rockbottom) {
            this.y = rockbottom;
            this.gravitySpeed = 0;
        }
    }

    //Permet de verifier si cet entité touche une autre
    this.crashWith = function(otherobj) {
        let myleft = this.x;
        let myright = this.x + this.width;
        let mytop = this.y;
        let mybottom = this.y + this.height;
        let otherleft = otherobj.x;
        let otherright = otherobj.x + (otherobj.width);
        let othertop = otherobj.y;
        let otherbottom = otherobj.y + (otherobj.height);
        let crash = true;
        if ((mybottom < othertop) || (mytop > otherbottom) || (myright < otherleft) || (myleft > otherright)) {
            crash = false;
        }
        return crash;
    }
}

//La boucle principale
function updateGameArea() {
    for (const obstacle of obstacles) {
        if (player.crashWith(obstacle)) {
            //game over
            document.location.reload(true);
            return;
        }
    }
    screen.clear(); // on clean l'ecran pour reafficher tout
    screen.frameNo += 1;

    for (const obstacle of obstacles) {
        obstacle.x -= 1;
        obstacle.update();
        if (obstacle.x < 0) obstacles.splice(obstacles.indexOf(obstacle), 1); //on supprime l'obstacle dès qu'il sort de l'ecran pour optimiser

    }

    //On affiche un nouvel obstacle à intervalle pseduo aleatoire
    if (screen.frameNo == 1 || (everyinterval(200) && Math.random() > 0.5)) {
        let screenWidth = screen.canvas.width;
        let screenHeight = screen.canvas.height;
        let minGap = 30;
        let maxGap = 80;
        let gap = Math.floor(Math.random()*(maxGap-minGap+1)+minGap);
        obstacles.push(new Component(10, screenHeight, "green", screenWidth, screenHeight-gap));
        speed--;
    }

    score.update();
    player.newPos();
    player.update();
}

//Procédure qui creer une nouvelle partie
function newGame() {
    player = new Component(playerSize, playerSize, "red", playerSize, screen.canvas.height-playerSize);
    player.gravity = 0.05;
    score = new Score("30px", "Corbel", "black", 280, 40);
    screen.start();
}

// Fonction qui permet au player de sauter
function jump(){
  if (!screen.interval) screen.interval = setInterval(updateGameArea, speed);
  if (player.y != screen.canvas.height-player.height) jumping = true;
  else jumping = false;
  if(!jumping){
    player.gravitySpeed = -3.5;
  }
}
