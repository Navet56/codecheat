package view;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Main JMenuBar class
 */
public class MainMenu extends JMenuBar{
    private JMenu file;
    private JMenu edit;
    private JMenu help;
    private JMenuItem newFile;
    private JMenuItem quit;
    private JMenuItem about;

    public MainMenu() {
        this.file = new JMenu("File");
        this.edit = new JMenu("Edit");
        this.help = new JMenu("Help");
        this.newFile = new JMenuItem("New file");
        this.quit = new JMenuItem("Quit");
        this.about = new JMenuItem("About");
        this.file.add(this.newFile);
        this.file.add(this.quit);
        this.add(this.file);
        this.add(this.edit);
        this.help.add(this.about);
        this.add(this.help);
    }

    public JMenuItem getNewFile() {
        return newFile;
    }

    public JMenuItem getQuit() {
        return quit;
    }

    public JMenu getFile() {
        return file;
    }

    public void setFile(JMenu file) {
        this.file = file;
    }

    public JMenu getEdit() {
        return edit;
    }

    public void setEdit(JMenu edit) {
        this.edit = edit;
    }

    public JMenu getHelp() {
        return help;
    }

    public void setHelp(JMenu help) {
        this.help = help;
    }

    public JMenuItem getAbout() {
        return about;
    }
}
