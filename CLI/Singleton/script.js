class ClassAvec1Instance {
	this.attribut = "rien";

	constructor() {
		if (ClassAvec1Instance.exists) {
		return ClassAvec1Instance.instance;
		}
		ClassAvec1Instance.instance = this;
		ClassAvec1Instance.exists = true;
		return this;
	}

}


objet = new ClassAvec1Instance();
objet2 = new ClassAvec1Instance();

System.out.println(objet.attribut);
objet.attribut = "Changement de l'attribut";
System.out.println(objet2.attribut); //a bien changé
