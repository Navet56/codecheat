import pygame,sys
from pygame.locals import *
import random

"""
Author : Navet56
Licence : GPL 3
"""
#colors
black = (  0,  0,  0)
white = (255,255,255)
green = (  0,  200,0)
red =   (255,  0,  0)
gray =  (200,200,200)

#Represente un bouton
class Bouton(object):
    #contructeur prenant en parametre la largeur la hauteur, le nom du bouton et sa position
    def __init__(self, w, h, name, x, y):
        self.text = pygame.font.SysFont('Corbel',35).render(name , True , black)
        self.xOrigin = x
        self.yOrigin = y
        self.width = w
        self.height = h
        self.rect = Rect(self.xOrigin,self.yOrigin,self.width,self.height)
        self.onMouse = False

    def update(self):
        mouse = pygame.mouse.get_pos()
        self.onMouse = self.xOrigin <= mouse[0] <= self.xOrigin+self.width and self.yOrigin <= mouse[1] <= self.yOrigin+self.height
        if self.onMouse:
            pygame.draw.rect(screen,white,self.rect)
        else:
            pygame.draw.rect(screen,gray,self.rect)
        screen.blit(self.text, (self.xOrigin+30,self.yOrigin+5))

#Creation de la fenetre
pygame.init()
screen = pygame.display.set_mode((800,400), 0, 32)
pygame.display.set_caption('Slider')
width = screen.get_width()
height = screen.get_height()

#Variables globales (à éviter le plus possible)
FPS = 30 #frames per second setting
fpsClock = pygame.time.Clock()
obstacles = []
playerSize = 40
floorHeight = int(height-height/6)

# Fonction qui retourne true toute les n frames
def everyinterval(n) :
    global frameNo
    global speed
    frameNo += 1
    if (frameNo / n) % 1 == 0:
        speed += 1
        return True
    else : return False

#Represente le score
class Score(object):
    """docstring for Score."""

    def __init__(self, size, font, color, x, y):
        self.score = 0
        self.size = size
        self.font = font
        self.color = color
        self.x = x
        self.y = y
        self.text = "Score : " + str(self.score)

    def update(self):
        self.score += 0.1
        self.text = "Score : " + str(int(self.score))
        screen.blit(pygame.font.SysFont(self.font,self.size).render(self.text, True , self.color), (self.x, self.y))

#Represente un obstacle (barre verte)
class Obstacle(object):
        def __init__(self):
            minGap = 30
            maxGap = 80
            gap = random.randint(minGap,maxGap)
            self.x = width
            self.height = gap
            self.width = 10
            self.y = floorHeight-self.height
            self.surf = pygame.Surface([self.width,self.height])
            self.surf.fill(green)

        def update(self):
            self.x -= speed
            screen.blit(self.surf, (int(self.x), int(self.y))) #affiche l'obstacle

#Represente le joueur (carré rouge)
class Player(object):
    #contructeur
    def __init__(self, w, h, color, x, y):
        self.width = w
        self.height = h
        self.x = h
        self.y = floorHeight-w
        self.surf = pygame.Surface([self.width,self.height])
        self.surf.fill(red)
        self.jumping = False
        self.up = True
    #Permet de verifier si le joueur touche un des obstacles
    def crash(self):
        for obstacle in obstacles :
            myleft = self.x
            myright = self.x + (self.width)
            mytop = self.y
            mybottom = self.y + (self.height)
            otherleft = obstacle.x
            otherright = obstacle.x + (obstacle.width)
            othertop = obstacle.y
            otherbottom = obstacle.y + (obstacle.height)
            if (mybottom < othertop) or (mytop > otherbottom) or (myright < otherleft) or (myleft > otherright) :
                return False
            #si true : game over
            return True

    def update(self):
        if self.crash() :
            newGame()
        if self.jumping :
            if self.up and self.y > 180:
                self.y -= speed
            else :
                self.up = False
                self.y += speed
            if self.y >= floorHeight-self.x :
                self.jumping = False
                self.up = True
                self.y = floorHeight-self.x
        screen.blit(self.surf, (int(self.x), int(self.y))) #affiche sur la screen l'image

bouton = Bouton(140,40, "JUMP", int(width/8), int(height-height/8))
player = Player(playerSize,playerSize, red, playerSize, floorHeight-playerSize)
score = Score(35,"Corbel", black ,int(width/3), 10)
#Procédure qui créer une nouvelle partie
def newGame():
    global frameNo
    global speed
    frameNo = 0
    speed = 10
    score.score = 0
    obstacles.clear()

# La boucle principale
def updateGameArea():
    while True:
        screen.fill(white) #on clean l'ecran pour reafficher tout
        screen.blit(pygame.Surface([width,1]),(0,floorHeight)) # le sol

        for obstacle in obstacles:
            obstacle.update()
            if obstacle.x < 0 : obstacles.remove(obstacle) #on supprime l'obstacle dès qu'il sort de l'ecran pour optimiser

        #on affiche un nouvel obstacle à intervalle pseudo aleatoire
        if everyinterval(20) and random.randint(0,1) == 1 :
            obstacles.append(Obstacle())

        #permet de prendre les evenemnts utilisateurs
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONUP and bouton.onMouse: #si l'utilisateur clique sur l'ecran et que il est positionné sur le bouton
                player.jumping = True #alors on jump

        bouton.update()
        player.update()
        score.update()

        pygame.display.flip() #refresh la screen
        pygame.display.update()
        fpsClock.tick(FPS)

newGame()
updateGameArea()
