<?php
require_once('SqliteConnection.php');
//require('Activity.php');

class ActivityEntryDAO {
    private static $dao;

    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new ActivityEntryDAO();
       }
       return self::$dao;
    }

    public final function findAll(){
       $dbc = SqliteConnection::getInstance()->getConnection();
       $query = "select * from Data";
       $stmt = $dbc->query($query);
       $results = $stmt->fetchAll();
       return $results;
    }

   public final function insert($st){
      if($st instanceof ActivityEntry){
         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "insert into data(time, cardio_frequency, longitude, latitude, altitude, my_activity) values (:t,:c,:lo,:la,:a,:ma)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':t',$st->getTime(),PDO::PARAM_STR);
         $stmt->bindValue(':c',$st->getCardio(),PDO::PARAM_STR);
         $stmt->bindValue(':lo',$st->getLongitude(),PDO::PARAM_STR);
         $stmt->bindValue(':la',$st->getLatitude(),PDO::PARAM_STR);
         $stmt->bindValue(':a',$st->getAltitude(),PDO::PARAM_STR);
         $stmt->bindValue(':ma',$st->getMyActivity(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
     }
  }

  public function delete($obj){ }

  public function update($obj){ }
}
?>
