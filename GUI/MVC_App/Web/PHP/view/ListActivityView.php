<?php
	require_once('model/Activity.php');
	require_once("BaseView.php");
    class ListActivityView {

        public static function render($request) {
            $html = BaseView::render(true) . "
             <div >
               <h1>Voici la liste des activités :</h1>
               " . $request . "
               <hr>
             </div>
            </body>
            </html>
            ";

            return $html;
        }

    }

?>
