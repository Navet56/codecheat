<?php
require('Controller.php');
require_once('view/DisconnectUserView.php');

    class DisconnectUserController implements Controller{

        public function handle($request){
			$view = new DisconnectUserView();
			if(isset($_SESSION['login'])){
				session_destroy();
				$response = "Déconnexion effectuée";
			} else {
				$response = "Aucune déconnexion à effectuer";
			}
			return $view->render($response);
        }
    }
?>
