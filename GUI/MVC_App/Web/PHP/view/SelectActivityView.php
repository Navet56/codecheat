<?php
	require_once('model/Activity.php');
	require_once("BaseView.php");
    class SelectActivityView {

        public static function render($request) {
            $html = BaseView::render(true) . "
             <div >
			<button type = \"button\"  onclick = \"history.back()\"> Retour à la liste des activités </button>

               <h1>Voici les informations supplémentaires de votre activité :</h1>
               " . $request . "
               <hr>
             </div>
            </body>
            </html>
            ";

            return $html;
        }

    }

?>
