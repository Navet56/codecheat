cd Python && python3 main.py
cd ../Java && javac Main.java && java Main
cd ../JavaScript && node script.js
cd ../C && gcc -c main.c && gcc -o main main.o && ./main
cd ../Cpp && g++ -c main.cpp && g++ -o main main.o && ./main
cd ../PHP && php script.php
