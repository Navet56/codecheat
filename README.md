# CodeCheat

Many code examples in many languages to be faster

The codes provided do not claim to be perfect. The conventions used try to be as clean and recent as possible, but errors can occur. If you find better ways to do this or that code, I invite you to Merge Request.

Sites to learn programming well:
MDN
Zestes-de-savoir
geeksforgeeks.org
FormationVidéo (Youtube)

tree :

    CLI
      |_ QuickSort
           |_ C++
           |_ Java
           |_ Python
           |_ JavaScript
           |_ PHP
           |_ C
      |_ Singleton
           |_ C++
           |_ Java
           |_ Python
           |_ JavaScript
           |_ PHP
      |_ Object-oriented code example
           |_ Python
           |_ Java
           |_ C++
           |_ JavaScript
      |_ TernaryOperation

    GUI
      |_ Image Movement
            |_ JavaFX
            |_ Pygame
            |_ Web (Javascript/CSS)
      |_ News Mobile Application
            |_ ReactJS
            |_ AngularJS
            |_ Python
            |_ Java
      |_ Slider platformer game
            |_ JavaScript
            |_ Godot
            |_ Pygame
            |_ Java Swing
      |_ Drawing a triangle
            |_ Java Swing
            |_ Pygame
            |_ Web (HTML/CSS)
            |_ SDL
            |_ OpenGL
            |_ SFML
            |_ Tkinter
      |_ MVC Example
            |_ Tkinter
            |_ Java Swing
            |_ Qt
            |_ Web
                |_ JavaScript (Express)
                |_ PHP
