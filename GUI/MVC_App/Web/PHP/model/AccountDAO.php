<?php
require_once('SqliteConnection.php');
require('Account.php');
class AccountDAO {
    private static $dao;

    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new AccountDAO();
       }
       return self::$dao;
    }

    public final function findAll(){
       $dbc = SqliteConnection::getInstance()->getConnection();
       $query = "select * from account";
       $stmt = $dbc->query($query);
       $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Account');
       return $results;
    }

    public final function findUser($email){
      $dbc = SqliteConnection::getInstance()->getConnection();
      $query = "select * from account WHERE email = '" . $email . "';";
      $stmt = $dbc->query($query);
      $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Account');
      return $results;
   }

   public final function insert($st){
      if($st instanceof Account){
         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "insert into account(name, first_name, birthdate, sex, height, weight, email, password) values (:l,:f,:b,:s,:h,:w,:e,:pw)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':l',$st->getLastname(),PDO::PARAM_STR);
         $stmt->bindValue(':f',$st->getFirstname(),PDO::PARAM_STR);
         $stmt->bindValue(':b',$st->getBirthDate(),PDO::PARAM_STR);
         $stmt->bindValue(':s',$st->getSex(),PDO::PARAM_STR);
         $stmt->bindValue(':h',$st->getHeight(),PDO::PARAM_STR);
         $stmt->bindValue(':w',$st->getWeight(),PDO::PARAM_STR);
         $stmt->bindValue(':e',$st->getEmail(),PDO::PARAM_STR);
         $stmt->bindValue(':pw',$st->getPassword(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
     }
  }

  public function delete($obj){ }

  public function update($account){
  if($account instanceof Account){
         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "update into account(name, first_name, birthdate, sex, height, weight, password) values (:l,:f,:b,:s,:h,:w,:pw)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':l',$st->getLastname(),PDO::PARAM_STR);
         $stmt->bindValue(':f',$st->getFirstname(),PDO::PARAM_STR);
         $stmt->bindValue(':b',$st->getBirthDate(),PDO::PARAM_STR);
         $stmt->bindValue(':s',$st->getSex(),PDO::PARAM_STR);
         $stmt->bindValue(':h',$st->getHeight(),PDO::PARAM_STR);
         $stmt->bindValue(':w',$st->getWeight(),PDO::PARAM_STR);
         $stmt->bindValue(':pw',$st->getPassword(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
     } }
}
?>
 
