<?php
	function quick_sort($my_array){
		$loe = $gt = array();
		if(count($my_array) < 2){
			return $my_array;
		}
		$pivot_key = key($my_array);
		$pivot = array_shift($my_array); // vire le premier element du tableau my_array et retourne cet élement (et donc ici le place dans pivot)
		foreach($my_array as $val){
			if($val <= $pivot){
				$loe[] = $val;
			} elseif ($val > $pivot){
				$gt[] = $val;
			}
		}
		return array_merge(quick_sort($loe),array($pivot_key=>$pivot),quick_sort($gt));//on quick sort des 2 cotés du pivot, et ça de façon recursive
	}
  $tab = [3,7,21,12,0,50,3,1,8,99]; //ou array(3,7,21,12,0,50,3,1,8,99) (syntaxe à éviter à l'avenir)
  $sortedTab = quick_sort($tab);
  echo "PHP : " . json_encode($sortedTab) . "\n"; //ou print_r("PHP : " . $sortedTab)
?>
