<?php
require_once('SqliteConnection.php');

class ActivityDAO {
    private static $dao;

    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new ActivityDAO();
       }
       return self::$dao;
    }

    public final function findAll(){
       $dbc = SqliteConnection::getInstance()->getConnection();
       $query = "select act_id, date, description, heureDeDebut, time(duree, 'unixepoch'), distance, cardio_min, cardio_moyenne, cardio_max, my_account from Activity";
       $stmt = $dbc->query($query);
       $results = $stmt->fetchAll();
       return $results;
    }


    public final function findEmail($key){
      $dbc = SqliteConnection::getInstance()->getConnection();
      $query = "select my_account from Activity where act_id = '" . $key . "';";
      $stmt = $dbc->query($query);
      $results = $stmt->fetchAll();
      return $results;
   }

   public final function findFromKey($key){
      $dbc = SqliteConnection::getInstance()->getConnection();
      $query = "select date, description, heureDeDebut, time(duree, 'unixepoch'), distance, cardio_min, cardio_moyenne, cardio_max from Activity where act_id = '" . $key . "';";
      $stmt = $dbc->query($query);
      $results = $stmt->fetchAll();
      return $results;
   }

   public final function insert($st){
      if($st instanceof Activity){

         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "insert into activity(date, description, heureDeDebut, duree, distance, cardio_min, cardio_moyenne, cardio_max, my_account) values (:da,:de,:h,:du,:di,:cmi,:cmo,:cma,:a)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':da',$st->getDate(),PDO::PARAM_STR);
         $stmt->bindValue(':de',$st->getDescription(),PDO::PARAM_STR);
         $stmt->bindValue(':h',$st->getHeureDebut(),PDO::PARAM_STR);
         $stmt->bindValue(':du',(string)$st->getDu(),PDO::PARAM_STR);
         $stmt->bindValue(':di',$st->getDistance(),PDO::PARAM_STR);
         $stmt->bindValue(':cmo',$st->getCardioMoyenne(),PDO::PARAM_STR);
         $stmt->bindValue(':cmi',$st->getCardioMin(),PDO::PARAM_STR);
         $stmt->bindValue(':cma',$st->getCardioMax(),PDO::PARAM_STR);
         $stmt->bindValue(':a',$st->getMyAccount(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
         $act_id = $dbc->lastInsertId();
         return $act_id;
     }
  }

  public function delete($obj){ }

  public function update($obj){ }
}
?>
