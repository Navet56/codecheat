import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class DrawTriangle {
    public static void main (String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame jFrame = new JFrame("Drawing a triangle in Swing");
            jFrame.setPreferredSize(new Dimension(800,800));
            jFrame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
            jFrame.add (new JPanel() {
                @Override
                public void paint(Graphics g) {
                    Graphics2D g2 = (Graphics2D) g;
                    g2.setColor(Color.RED);
                    //Vertex 1 : x=200, y=600
                    //Vertex 2 : x=400, y=200
                    //Vertex 3 : x=600, y=600
                    g2.fillPolygon(new int[]{200, 400, 600}, new int[]{600, 200, 600},3);
                }
                });
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jFrame.pack();
            Dimension screenSize = Toolkit.getDefaultToolkit ().getScreenSize ();
            Dimension size = jFrame.getSize();
            jFrame.setLocation ((screenSize.width - size.width)/4, (screenSize.height - size.height)/4);
            jFrame.setVisible(true);
        });
    }
}
