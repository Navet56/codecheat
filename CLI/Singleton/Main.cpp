#include <iostream>

class ClassAvec1Instance
{
    private:
        static ClassAvec1Instance* instance; //le singleton
        //contructeur privé
        ClassAvec1Instance();

    public:
        static ClassAvec1Instance* getInstance();
};

//instance = nullptr (pointeur nul)
ClassAvec1Instance* ClassAvec1Instance::instance = nullptr;

ClassAvec1Instance* ClassAvec1Instance::getInstance() {
    if (instance == nullptr){
        instance = new ClassAvec1Instance();
    }
    return instance;
}

ClassAvec1Instance::ClassAvec1Instance(){}

int main(){
    //new ClassAvec1Instance(); // Ne doit pas marcher
    ClassAvec1Instance* s = ClassAvec1Instance::getInstance();
    ClassAvec1Instance* r = ClassAvec1Instance::getInstance();
    
    delete s, r;
    //Doit retourner les memes adresses
    std::cout << s << std::endl;
    std::cout << r << std::endl;
    return(0);
}
