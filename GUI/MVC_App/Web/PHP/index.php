
<?php
 session_start();
 require ('control/ApplicationController.php');
 require ('model/Activity.php');
 require_once('model/ActivityDAO.php');
 


 if (!isset($_REQUEST['page'])){
    $_REQUEST['page'] = "welcome";
 }

 $c = ApplicationController::getInstance()->getController($_REQUEST);
 if ($c != null){
	
    $ct = 'control/' . $c . '.php';
    if (($ct == "control/ListActivityController.php" || $ct == "control/UploadActivityController.php" || $ct == "control/MyAccountController.php") && !isset($_SESSION['login'])) {
		$ct = 'control/ErrorController.php';
		$c = "ErrorController";
    }
    require_once($ct);
    $ctrl = new $c();
    echo $ctrl->handle($_REQUEST);
 }



?>
