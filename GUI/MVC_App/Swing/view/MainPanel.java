package view;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {

    private JLabel label1;
    private JLabel labelo;
    private JButton bouton;
    private JPanel panel1;
    private JPanel panel2;

    public MainPanel() {
        this.setLayout(new GridLayout(1,1));
        this.setBackground(Color.WHITE); // Couleur du fond de la fenetre en blanc
        this.panel1 = new JPanel(); // Creation d'un panel
        this.panel2 = new JPanel(); // Creation d'un autre panel
        this.label1 = new JLabel(); // Creation d'un label (c'est à dire un texte)
        panel1.add(this.label1); // Ajout de ce label au panel 1
        this.labelo = new JLabel(); // Creation d'un nouveau label
        panel2.add(labelo); // Ajout de ce label au panel 2
        this.setLayout(new BorderLayout()); // Definition de la strategie de placement des élements, ici on choisie le placement BorderLayout pour pouvoir placer en choisssant une direction
        this.add(panel1, BorderLayout.WEST); // Ajout du panel 1 à gauche de la fenetre
        this.add(panel2, BorderLayout.EAST); // Ajout du panel 2 à droite de la fenetre
        this.bouton = new JButton("Fermer"); // Creation d'un bouton avec comme texte : "Fermer"
        bouton.setBackground(Color.BLACK); // Couleur de l'arrière plan en blanc (couleur du fond)
        bouton.setForeground(Color.WHITE); // Couleur de premier plan en blanc (couleur de la police)
        bouton.addActionListener(e -> System.exit(0)); // La commande du bouton ferme la fenetre
        this.add(bouton, BorderLayout.SOUTH); // Ajout du bouton dans la fenetre et collé tout en bas

    }

    public JLabel getLabel1() {
        return this.label1;
    }
    public JLabel getLabelo() {
        return this.labelo;
    }
}
