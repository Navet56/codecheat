class Singleton:

    def __init__(self, decorated):
        self._decorated = decorated

    def Instance(self):
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)

#utilisation d'une decoration
@Singleton
class ClassAvec1Instance:
    def __init__(self):
        self.name=None
        self.val=0
    def getName(self):
        return self.name

x = ClassAvec1Instance.Instance()
y = ClassAvec1Instance.Instance()

print(y.getName())
x.name = "Changement de l'attribut de l'objet x"
print(y.getName()) #on voit donc que x = y = l'unique instance de la classe
