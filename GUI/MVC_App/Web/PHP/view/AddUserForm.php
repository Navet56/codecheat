<?php
require_once('BaseView.php');
	
class AddUserForm {

  public static function render() {
    $html = BaseView::render(false) .
    "
      <form action=\"?page=validuser\" method=\"post\">
      <div class=\"creercompte\">
        <h1>Inscrivez vous</h1>
        <p>Merci de remplir ce formulaire pour créer un compte</p>
        <hr>
            
        <label for=\"nom\"><b>Nom</b></label>
        <input type=\"text\" placeholder=\"Entrer votre nom\" name=\"nom\" pattern=\"[A-Z](?=.*[a-z]).{2,30}\" required>
            
        <label for=\"prenom\"><b>Prenom</b></label>
        <input type=\"text\" placeholder=\"Entrer votre prenom\" name=\"prenom\" required>
            
        <label for=\"email\"><b>Email</b></label>
        <input type=\"text\" placeholder=\"Entrer votre mail\" name=\"email\" required>
            
        <label for=\"sexe\"><b>Sexe</b></label>
        <select id=\"sexe\" name=\"sexe\">
          <option value=\"masculin\"> Masculin</option>
          <option value=\"feminin\"> Feminin</option>
          <option value=\"autre\"> Autre </option>
        </select>
        <br>
        <label for=\"taille\" ><b>Taille</b></label>
        <input type=\"number\" placeholder=\"en cm\" id=\"taille\" name=\"taille\" min=\"100\" max=\"272\">
            
        <label for=\"poids\" ><b>Poids</b></label>
        <input type=\"number\" placeholder=\"en kg\" id=\"poids\" name=\"poids\" min=\"0\" max=\"300\">
            
            
        <label for=\"psw\"><b>Mot de passe</b></label>
        <input type=\"password\" placeholder=\"Entrez un mot de passe sécurisé\" name=\"mdp\" minlength=7 maxlength=30  pattern=\"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{7,30}\" required>
            
        <label for=\"psw-repeat\"><b>Entrer à nouveau le mot de passe</b></label>
        <input type=\"password\" placeholder=\"Même mot de passe\" name=\"repeat-mdp\" required>
            
        <label for=\"date\"><b>Date de naissance</b></label>
        <input type=\"date\" name=\"date\" required>
            
        <label>
          <input type=\"checkbox\" checked=\"checked\" name=\"rappeller\" style=\"margin-bottom:15px\"> Se rappeller de moi
        </label>
            
        <div>
          <button type=\"submit\" class=\"creercomptebtn\">Créer son compte</button>
        </div>
      </div>
    </form>
  </body>
</html>
";

            return $html;
        }

    }

?>
