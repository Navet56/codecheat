import pygame,sys
from pygame.locals import *

pygame.init()

FPS = 25 #frames per second setting
fpsClock = pygame.time.Clock()

#set up the window
fenetre = pygame.display.set_mode((800,800), 0, 32)
pygame.display.set_caption('Image Movement')

#set up the colors
black = (  0,   0,   0)
white = (255, 255, 255)
red   = (255,   0,   0)
green = (  0, 255,   0)
blue  = (  0,   0, 255)

imageImg  = pygame.image.load('mario.png')#.convert()
image = imageImg.convert_alpha()
imagex = 320
imagey = 220

# the main game loop
while True:
    fenetre.fill(green) #background color = green

    imagex+=1
    imagey+=1
    if(imagex > 800) :
	    imagex = 0
	    imagey = 0
    fenetre.blit(imageImg, (imagex, imagey)) #affiche sur la fenetre l'image
    pygame.display.flip() #refresh la fenetre

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

pygame.display.update()
fpsClock.tick(FPS)
