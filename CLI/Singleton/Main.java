public class ClassAvec1Instance {
  private ClassAvec1Instance instance;
  private String attribut;

  private ClassAvec1Instance(){
    this.attribut = "rien";
  }

  public static ClassAvec1Instance getInstance(){
    if(instance==null) this.instance = new ClassAvec1Instance();
    return this.instance;
  }

  public void setAttribut(String s){
    this.attribut = s;
  }

  public String getAttribut(){
    return attribut;
  }

  public static void main(String[] args) {
    ClassAvec1Instance objet = ClassAvec1Instance.getInstance();
    ClassAvec1Instance objet2 = ClassAvec1Instance.getInstance();

    System.out.println(objet.getAttribut());
    objet.setAttribut("Changement de l'attribut");
    System.out.println(objet2.getAttribut()); //a bien changé

  }

}
