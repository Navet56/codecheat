DELETE FROM Planning;

DROP TABLE Planning;

CREATE TABLE Planning(
    id INTEGER(10) PRIMARY KEY,
    name TEXT,
    weekend BOOL,
    dark_theme BOOL,
    easy_mode BOOL,
    tasks TEXT
);
