import 'dart:math';

class Person {
  int taille = 0;
  String name = "default";
  Person(int taille, {String name = "default"}){
    this.taille = taille;
    this.name = name;
  }

  String toString() => "Person( $name est de taille $taille )";

}

class Persons {
  var personsList = [];

  Persons.withListInit(List<Person> plist){
    personsList = plist;
  }

  /**
   * Défault constructor
   */
  Persons(){

  }
 

  dynamic getList(){
    return personsList;
  }

  void addPerson(Person p){
    personsList.add(p);
  }

  dynamic popPerson(){
    return personsList.removeLast();
  }
}


void main() {
  Person kilian = Person(190, name:"Kilian");
  var randomMec = Person(Random().nextInt(100)+100);

  print(kilian);

  print(randomMec);

  Persons listPersons = Persons();

  listPersons.addPerson(kilian);
  listPersons.addPerson(randomMec);

  print(listPersons.popPerson());

  print(listPersons.getList());

  var listPersons2 = Persons.withListInit([kilian, randomMec]);

  print(listPersons2.getList());

  print("Début for each tailles :");

  listPersons2.getList().forEach(
    (p) =>  {
      print("    Taille : ${p.taille}")
    }
  );


}
