public class Human {
  private String nom;
  private String prenom;
  private int age;

  public Human(){
    this.nom = "Test";
    System.out.println("Naissance de l'humain " + this.nom);
  }

  public void methode(){
    System.out.println("Une methode de la classe Human");
  }

  public String getName(){
    System.out.println(this.nom);
    return this.nom;
  }
}
