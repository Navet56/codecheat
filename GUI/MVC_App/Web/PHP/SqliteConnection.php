<?php
      class SqliteConnection {
        public static $pdo;
        public static $sqlite;
        private function __contruct(){

        }

        //singleton pattern
        public final static function getInstance() {
          if(!isset(self::$sqlite)) {
              self::$sqlite = new SqliteConnection();
          }
          return self::$sqlite;
       }

        public function getConnection(){
          if (!isset(self::$pdo)) {
            try{
              self::$pdo = new PDO('sqlite:db/sport_track.db');
              self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(Exception $e) {
              echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
              die();
              }
            self::$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          }
          return self::$pdo;
        }

      }

?>
