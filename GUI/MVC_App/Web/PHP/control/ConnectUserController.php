<?php
require('Controller.php');
require('view/ConnectUserForm.php');

    class ConnectUserController implements Controller{

        public function handle($request){
			$view = new ConnectUserForm();
			return $view->render();
        }
    }
?>
