<?php
class ClassAvec1Instance {
    private static $instance;
    private $attribut;

    private function __construct() {
      $this->attribut = "rien";
    }

    public final static function getInstance() {
       if(!isset(self::$instance)) {
           self::$instance= new ClassAvec1Instance();
       }
       return self::$instance;
    }

    public function getAttribut(){
      return $this->attribut;
    }

    public function setAttribut($a){
      $this->attribut = $a;
    }
}

$objet = ClassAvec1Instance::getInstance();
$objet2 = ClassAvec1Instance::getInstance();

System.out.println($objet->getAttribut());
$objet->setAttribut("Changement de l'attribut");
System.out.println($objet2->getAttribut()); //a bien changé

?>
